drop DATABASE if EXISTS todo;

create database todo;

use todo;



create table user (
    id int PRIMARY key auto_increment,
    username varchar(30) not null UNIQUE,
    password VARCHAR(255) not NULL, 
    firstname VARCHAR(100),
    lastname VARCHAR(100)
);


create table task (
    id int PRIMARY key AUTO_INCREMENT,
    title varchar(255) not null,
    added timestamp DEFAULT CURRENT_TIMESTAMP,
    description text,
    user_id int not null,
    index (user_id),
    foreign key (user_id) references user(id)
    on delete restrict
    );
    