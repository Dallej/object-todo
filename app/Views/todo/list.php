<h3><?= $title ?></h3>

<?= anchor('todo/create','Add new task')?>
<table class="table">
    <tr>
        <th>Title</th>
        <th>User</th>
        <th>Description</th>
        <th></th>
    </tr>
<?php foreach ($todos as $todo): ?>
    <tr>
        <th><?=$todo['title'] ?></th>
        <th><?= $todo['firstname'] . " " . $todo['lastname']?></th>
        <th><?= $todo['description']?></th>
        <th><?= anchor('todo/delete/' . $todo['id'], 'delete')?></th>
    </tr>
<?php endforeach;?>
</table>
  